import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Photo } from "src/app/shared/models/photo";
import { MessageService } from "src/app/core/services/message.service";
import { Book } from "src/app/features/edit/models/book";

@Injectable({
    providedIn: "root",
})
export class PhotoService {
    private baseUrl = "http://localhost:8080/photo";

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {}

    /** Log a PhotoService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`PhotoService: ${message}`);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */

    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    getPhotos(): Observable<Photo[]> {
        return this.http.get<Photo[]>(`${this.baseUrl}/getAllPhotos`).pipe(
            tap((_) => this.log("Books fetched")),
            catchError(this.handleError<Photo[]>("getBooks", []))
        );
    }

    savePhotoInfo(book: Book, file: File): Observable<any> {
        const formData: FormData = new FormData();
        formData.append("file", file);
        //This is need when using FormData to send json along with file
        const blobOverrides = new Blob([JSON.stringify(book)], {
            type: "application/json",
        });
        formData.append("book", blobOverrides);
        console.log(`book in photoService${JSON.stringify(book)}`);

        return this.http
            .post<any>(`${this.baseUrl}/save`, formData, {
                reportProgress: true,
                observe: "events",
            })
            .pipe(
                tap(() => this.log(`book info added}`)),
                catchError(this.handleError<Book>("savePhotoInfo"))
            );
    }
    // Spring do not support Put for MultipartFile. Need To make a post
    updatePhotoInfo(book: Book, file: File, photoId: String): Observable<any> {
        const formData: FormData = new FormData();
        formData.append("file", file);
        //This is need when using FormData to send json along with file
        const blobOverrides = new Blob([JSON.stringify(book)], {
            type: "application/json",
        });
        formData.append("book", blobOverrides);
        formData.append("photoId", "photoId");

        return this.http
            .post<Photo>(`${this.baseUrl}/update`, formData, {
                reportProgress: true,
                observe: "events",
            })
            .pipe(
                tap(() => this.log(`book info updated`)),
                catchError(this.handleError<Photo>("updatePhotoInfo"))
            );
    }

    findPhotoById(id: String): Observable<Photo> {
        const url = `${this.baseUrl}/getphoto/${id}`;
        return this.http.get<Photo>(url).pipe(
            tap((_) => this.log(`findPhotoById id=${id}`)),
            catchError(this.handleError<Photo>(`findPhotoById id=${id}`))
        );
    }

    deletePhotoById(id: String): Observable<{}> {
        const url = `${this.baseUrl}/deletePhoto/${id}`;
        return this.http.delete(url).pipe(
            tap((_) => this.log(`deleted book id=${id}`)),
            catchError(this.handleError<Book>("deleteBook"))
        );
    }

    /* GET heroes whose name contains search auteur */
    searchPhotosInfo(auteur: string): Observable<Photo[]> {
        if (!auteur.trim()) {
            // if not search auteur, return empty hero array.
            return of([]);
        }
        return this.http.get<Photo[]>(`${this.baseUrl}/getPhotoInfoByAuteur/${auteur}`).pipe(
            tap((x) =>
                x.length
                    ? this.log(`found heroes matching "${auteur}"`)
                    : this.log(`no heroes matching "${auteur}"`)
            ),
            catchError(this.handleError<Photo[]>("searchPhotosInfo", []))
        );
    }
}
