import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BookService } from "./features/edit/services/book.service";
import { PageNotFoundComponent } from "./core/components/page-not-found/page-not-found.component";
import { HeaderComponent } from "./core/components/header/header.component";
import { FooterComponent } from "./core/components/footer/footer.component";
import { CoreModule } from "./core/core.module";
import { SearchBookComponent } from './features/book-list/components/search-book/search-book.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        PageNotFoundComponent,
    ],
    imports: [
        CoreModule,
        AppRoutingModule,
        NgbModule,
    

    ],
    providers: [BookService],
    bootstrap: [AppComponent],
})
export class AppModule {}
