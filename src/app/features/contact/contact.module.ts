import { NgModule } from '@angular/core';
import { ContactRoutingModule } from './contact-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContactPageComponent } from './components/contact-page/contact-page.component';
import { ContactComponent } from './contact.component';


@NgModule({
  declarations: [
    ContactComponent,
    ContactPageComponent
  ],
  imports: [
    SharedModule,
    ContactRoutingModule
  ]
})
export class ContactModule { }
