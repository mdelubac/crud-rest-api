import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BookComponent } from './components/book/book.component';


@NgModule({
  declarations: [
    EditComponent,
    BookComponent
  ],
  imports: [
    SharedModule,
    EditRoutingModule
  ]
})
export class EditModule { }
