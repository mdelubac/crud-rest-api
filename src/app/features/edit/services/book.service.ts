import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Book } from "../models/book";
import { MessageService } from "../../../core/services/message.service";

@Injectable({
    providedIn: "root",
})
export class BookService {
    private baseUrl = "http://localhost:8080/bibliotheque";

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {}

    /** Log a BookService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`BookService: ${message}`);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */

    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    getBooks(): Observable<Book[]> {
        return this.http.get<Book[]>(`${this.baseUrl}/livres`).pipe(
            tap((_) => this.log("Books fetched")),
            catchError(this.handleError<Book[]>("getBooks", []))
        );
    }

    saveBook(book: Book): Observable<any> {
        return this.http.post<any>(`${this.baseUrl}/save`, book).pipe(
            tap((newBook: Book) =>
                this.log(`added book id=${newBook.id}`)
            ),
            catchError(this.handleError<Book>("addBook"))
        );
    }

    deleteBook(id: number): Observable<{}> {
        const url = `${this.baseUrl}/delete/${id}`;
        return this.http.delete(url).pipe(
            tap((_) => this.log(`deleted book id=${id}`)),
            catchError(this.handleError<Book>("deleteBook"))
        );
    }

    findBookById(id: number): Observable<any> {
        const url = `${this.baseUrl}/findbyid/${id}`;
        return this.http.get<any>(url).pipe(
            tap((_) => this.log(`findBookById id=${id}`)),
            catchError(this.handleError<Book>(`findBookById id=${id}`))
        );
    }

    updateBook(livre: Book): Observable<any> {
        return this.http.put<any>(`${this.baseUrl}/update`, livre).pipe(
            tap((_) => this.log(`update book id=${livre.id}`)),
            catchError(this.handleError<any>("updateBook"))
        );
    }
}
