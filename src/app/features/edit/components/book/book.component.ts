import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Book } from "src/app/features/edit/models/book";
import { BookService } from "src/app/features/edit/services/book.service";

@Component({
    selector: "app-book",
    templateUrl: "./book.component.html",
    styleUrls: ["./book.component.css"],
})
export class BookComponent implements OnInit {
    bookForm = new FormGroup({
        id: new FormControl(""),
        titre: new FormControl(""),
        auteur: new FormControl(""),
        datePublication: new FormControl(""),
    });

    books: Book[];
    book: Book;

    constructor(private bookService: BookService) {}

    ngOnInit(): void {
        this.getBooks();
    }

    getBooks(): void {
        this.bookService.getBooks().subscribe((data) => (this.books = data));
    }

    save(): void {
        this.book = this.bookForm.value;
        this.bookService.saveBook(this.book).subscribe((book) => {
            this.books.push(book);
        });
    }

    delete(book: Book): void {
        this.bookService.deleteBook(book.id).subscribe(() => {
            this.books = this.books.filter(
                (bookInList) => bookInList.id !== book.id
            );
        });
    }

    findById(id: number): any {
        return this.bookService.findBookById(id).subscribe((data) => {
            this.book = data;
            this.bookForm.setValue({
                id: this.book.id,
                titre: this.book.titre,
                auteur: this.book.auteur,
                datePublication: this.book.datePublication,
            });
        });
    }
}
