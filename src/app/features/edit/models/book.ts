export interface Book {
    id  : number;
    titre : String;
    auteur : String;
    datePublication : String;
}
