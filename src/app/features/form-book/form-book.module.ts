import { NgModule } from '@angular/core';
import { FormBookRoutingModule } from './form-book-routing.module';
import { FormBookComponent } from './form-book.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormBookInfoComponent } from './components/form-book-info/form-book-info.component';


@NgModule({
  declarations: [
    FormBookComponent,
    FormBookInfoComponent
  ],
  imports: [
    SharedModule,
    FormBookRoutingModule
  ]
})
export class FormBookModule { }
