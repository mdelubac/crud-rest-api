import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormBookComponent } from './form-book.component';

const routes: Routes = [{ path: '', component: FormBookComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormBookRoutingModule { }
