import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBookInfoComponent } from './form-book-info.component';

describe('FormBookInfoComponent', () => {
  let component: FormBookInfoComponent;
  let fixture: ComponentFixture<FormBookInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBookInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBookInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
