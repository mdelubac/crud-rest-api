import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PhotoService } from 'src/app/core/services/photo.service';
import { Book } from 'src/app/features/edit/models/book';
import { Photo } from 'src/app/shared/models/photo';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form-book-info',
  templateUrl: './form-book-info.component.html',
  styleUrls: ['./form-book-info.component.css']
})
export class FormBookInfoComponent implements OnInit {
  
  bookForm = new FormGroup({
    id: new FormControl(""),
    titre: new FormControl(""),
    auteur: new FormControl(""),
    datePublication: new FormControl(""),
});

fileName : String ='';
file : File;
uploadProgress:number;

  constructor(
    private photoService: PhotoService) { }

  ngOnInit(): void {  
  }

  onFileSelected(event){
    const file : File= event.target.files[0];
    if(file){
      this.fileName = file.name;
      this.file =file;
    }
  }

  save(): void {
    this.photoService.savePhotoInfo(this.bookForm.value, this.file).subscribe((event) => {
      if (event.type == HttpEventType.UploadProgress) {
        this.uploadProgress = Math.round(100 * (event.loaded / event.total));
      }
    });
    
}


}
