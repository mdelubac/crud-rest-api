import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Book } from 'src/app/features/edit/models/book';
import { Photo } from 'src/app/shared/models/photo';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { map, switchMap, tap } from 'rxjs/operators';
import { PhotoService } from 'src/app/core/services/photo.service';
import { HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})
export class EditFormComponent implements OnInit {

  bookForm = new FormGroup({
    id: new FormControl(""),
    titre: new FormControl(""),
    auteur: new FormControl(""),
    datePublication: new FormControl(""),
});

fileName : String ='';
file : File;
uploadProgress:number;
photoId : String;
photoData : Photo["photoData"]; //indexed acces type since Typecript 2.1
photo$ : Observable<Photo>;

  constructor(
    private route : ActivatedRoute,
    private router : Router,
    private photoService: PhotoService
  ) { }

  ngOnInit(): void {
    this.photo$=this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.photoService.findPhotoById(params.get('photoId')!)),
      tap((photo$) => this.bookForm.setValue({
        id: photo$.book.id,
        titre: photo$.book.titre,
        auteur: photo$.book.auteur,
        datePublication: photo$.book.datePublication,
      })),
      tap((photo$)=> this.photoData =photo$.photoData),
      tap((photo$)=> this.photoId =photo$.photoId)
     
    );
  }

  onFileSelected(event){
    const file : File= event.target.files[0];
    if(file){
      this.fileName = file.name;
      this.file =file;
    }
  }

  updateBookInfo(): void {
    console.log(JSON.stringify(`value from component => bookfrom : ${this.bookForm.value}`));
    console.log(JSON.stringify(`value from  component => photoId : ${this.photoId}`));
    this.photoService.updatePhotoInfo(this.bookForm.value, this.file, this.photoId).subscribe((event) => {
      if (event.type == HttpEventType.UploadProgress) {
        this.uploadProgress = Math.round(100 * (event.loaded / event.total));
      }
    });
    
}

goBackToBooks() : void{
  const navPhotoId =  this.photoId? this.photoId :null;
  this.router.navigate(['/books', {id: navPhotoId}]);
}



}
