import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookDetailEditRoutingModule } from './book-detail-edit-routing.module';
import { BookDetailEditComponent } from './book-detail-edit.component';
import { EditComponent } from '../edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditFormComponent } from './components/edit-form/edit-form.component';


@NgModule({
  declarations: [
    BookDetailEditComponent,
    EditFormComponent
  ],
  imports: [
    SharedModule,
    BookDetailEditRoutingModule
  ]
})
export class BookDetailEditModule { }
