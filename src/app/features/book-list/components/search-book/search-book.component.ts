import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { PhotoService } from 'src/app/core/services/photo.service';
import { Photo } from 'src/app/shared/models/photo';

@Component({
  selector: 'app-search-book',
  templateUrl: './search-book.component.html',
  styleUrls: ['./search-book.component.css']
})
export class SearchBookComponent implements OnInit {

  photos$!: Observable<Photo[]>;
  private searchTerms = new Subject<string>();

  constructor(private photoService : PhotoService) { }

  ngOnInit(): void {
    this.photos$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((auteur: string) => this.photoService.searchPhotosInfo(auteur)),
    );
  }

    // Push a search term into the observable stream.
    search(auteur: string): void {
      this.searchTerms.next(auteur);
    }

}
