import { Component, OnInit } from "@angular/core";
import { Photo } from "../../../../shared/models/photo";
import { PhotoService } from "../../../../core/services/photo.service";
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
    selector: "app-list-cards",
    templateUrl: "./list-cards.component.html",
    styleUrls: ["./list-cards.component.css"],
})

export class ListCardsComponent implements OnInit {
    // Not using Async pipe so I should unsuscribe to observales !!!
    photos: Photo[];
    photo : Photo;
    selectedId;

    constructor(
        private photoService: PhotoService,
        private route :ActivatedRoute

    ) {}

    ngOnInit(): void {
    
        this.selectedId = this.route.snapshot.paramMap.get('id')!;
        this.getAllPhotos();
    }

    getAllPhotos(): void {
        this.photoService.getPhotos().subscribe((data) => {
            this.photos = data;
        });
    }
    
    deletePhotoById(id : String): void{
        this.photoService.deletePhotoById(id).subscribe();
        this.photos = this.photos.filter(
            (photo) => photo.photoId!==id
        );
    }
}
