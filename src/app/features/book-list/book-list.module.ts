import { NgModule } from '@angular/core';
import { BookListRoutingModule } from './book-list-routing.module';
import { BookListComponent } from './book-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListCardsComponent } from './components/list-cards/list-cards.component';
import { SearchBookComponent } from './components/search-book/search-book.component';

@NgModule({
  declarations: [
    BookListComponent,
    ListCardsComponent,
    SearchBookComponent
  ],
  imports: [
    BookListRoutingModule,
    SharedModule,
  ]
})
export class BookListModule { }
