import { Component, OnInit } from '@angular/core';
import{ trigger, state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-heart-animation',
  animations:[
    trigger('heartBeat', [

      state('relaxation', style({
        height :'400px',
        backgroundColor: 'yellow'
      })),

      state('contraction', style({
        height :'200px',
        backgroundColor: 'red'

      })),
      transition('contraction <=>relaxation', [
        animate('3s ease-out')
      ]),
    ]),
  ],
  templateUrl: './heart-animation.component.html',
  styleUrls: ['./heart-animation.component.css']
})
export class HeartAnimationComponent implements OnInit {
  isRelaxed : boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggle() {
    this.isRelaxed = !this.isRelaxed;
  }
}
