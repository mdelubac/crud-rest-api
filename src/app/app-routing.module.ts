import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
  { path: 'books', loadChildren: () => import('./features/book-list/book-list.module').then(m => m.BookListModule) },
  { path: 'form-book', loadChildren: () => import('./features/form-book/form-book.module').then(m => m.FormBookModule) },
  { path: 'contact', loadChildren: () => import('./features/contact/contact.module').then(m => m.ContactModule) },
  { path: 'edit', loadChildren: () => import('./features/edit/edit.module').then(m => m.EditModule) },
  { path: 'book-detail-edit/:photoId', loadChildren: () => import('./features/book-detail-edit/book-detail-edit.module').then(m => m.BookDetailEditModule) },
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
