import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Pipe({
    name: "safeResourceUrl",
})
export class SafeResourceUrlPipe implements PipeTransform {
  
    constructor(private sanitizer: DomSanitizer) {}

    transform(value: unknown, ...args: unknown[]): unknown {
        return this.sanitizer.bypassSecurityTrustResourceUrl("data:image/png;base64," + value);
    }
}
