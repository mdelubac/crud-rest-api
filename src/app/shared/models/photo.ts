import { Book } from "../../features/edit/models/book";

export interface Photo {
    photoId : String;
    photoName  : String;
    photoType : String;
    photoData : any;
    book : Book;
}
