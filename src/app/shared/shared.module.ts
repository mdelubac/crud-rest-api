import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafeResourceUrlPipe } from './pipes/safe-resource-url.pipe';
import { MessagesComponent } from './components/messages/messages.component';

@NgModule({
  declarations: [
    MessagesComponent,
    SafeResourceUrlPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

  ], 
  exports:[
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SafeResourceUrlPipe,
    MessagesComponent


  ]
})
export class SharedModule { }
